# -*- coding: utf-8 -*-
from os import path
from vk_api import vk_api, VkUpload, VkTools
from random import randint

# to get access token visit vkhost.github.io
# Access rights required: docs. 
ACCESS_TOKEN = ''

# id to which is sent
owner_id = ID

vk_session = vk_api.VkApi(token=ACCESS_TOKEN)
vk = vk_session.get_api()

# formats: Ogg Opus.
# Limits: sample rate 16kHz, variable bitrate 16 kbit/s, duration no more 5 minutes. 
path = path.join(path.dirname(__file__), 'AUDIO.mp3')


def main():
    upload = VkUpload(vk_session)

    audio_msg = upload.audio_message(path, peer_id=owner_id)
    audio_msg_id = audio_msg['audio_message']['id']
    
    vk.messages.send(
                    user_id=owner_id, 
                    random_id=getRandomInt64(), 
                    peer_id=owner_id,
                    attachment=f'audio_message{owner_id}_{audio_msg_id}'
    )
    
def getRandomInt64():
    return randint(100000000000000000,9223372036854775807)

if __name__ == '__main__':
    main()